import java.util.HashMap;
import java.util.Map;


/**
 * Main application
 * @author taweesoft
 * 16.05.2015.
 */
public class Main {
	public static void main(String[] args) {
		Person p = new Person("Adam",1);
		Person p2 = new Person("Adam",1);
		
		Map<Person,String> map = new HashMap<Person,String>();
		
		map.put(p, "Adam");
		
		// Why it true ? I didn't put p2 into the map.
		System.out.println(map.containsKey(p2)); 
		
		/**
		 * Because HashMap has no elements. It collect the value like a bucket.
		 * Same object will collect in same bucket.
		 * Same object is mean same hashcode.
		 * So when containsKey(key) has called.
		 * It will check at equals() of each object and in equals() on Person
		 * that check hashcode is same.
		 * In this problem we create 2 instances of object but same information.
		 * Same information is same hashcode that from hashCode() that overriden
		 * in Person.
		 * Then map will give you a true because 2 objects have the same hashcode.
		 */
	}
}
