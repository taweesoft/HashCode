
/**
 * Person.
 * @author taweesoft
 *
 */
public class Person {
	private String name;
	private int age ;
	
	public Person(String name,int age){
		this.name = name;
		this.age = age;
	}
	
	public void setAge(int age){
		this.age = age;
	}
	
	/**
	 * The way to calculate hashcode.
	 * 1. Initialize some number such as result = 17;
	 * 2. Find hashcode of each instance variable.
	 * 		2.1 name : result = result * 37 + name.hashCode();
	 * 		2.2  age : result = result * 37 + (int)age;
	 * 3. return result.
	 */
	@Override
	public int hashCode(){
		int result = 17; // some number.
		result = result*37 + name.hashCode();
		result = result*37 + age;
		return result;
	}
	
	/**
	 * Equals.
	 * If same object that mean hashcode is equals.
	 * But hashcode is equals isn't mean same object.
	 */
	@Override
	public boolean equals(Object obj){
		if(obj == null) return false;
		if(obj.getClass() != Person.class) return false;
		
		Person p = (Person)obj;
		
		return hashCode() == p.hashCode();
	}
}
